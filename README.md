# Examen Práctico: Backend PHP

El banco nacional **PCOM**  desea implementar un **Api Rest** que  permita  la apertura de cuentas bancarias y su historial de movimientos.

- Todos los servicios deben contar con  autentificación **oAuth**
- Apertura de cuentas: El servicio debe crear una cuenta bancaria con los datos mínimos requeridos que a continuación se especifican:
	- Nombre, apellido y correo del usuario
	- Tipo de cuenta, monto y fecha de apertura
	- En caso de no existir el usuario proporcionado se debe crear  con una contraseña aleatoria
- Transacciones: Se requiere que el Api permita  diferentes movimientos bancarios que el usuario requiera.
	- El usuario debe proporcionar  su número de cuenta.
	- Las transacciones se realizarán bajo los siguientes criterios: 
		- Los cargos serán identificados como un **tipo 1** y se le agregara un IVA del 16%
		- Lo depositos de identifican como un movimiento **tipo 2**
		- Todas las transacciones deben contar con una descripción
- Historial de movimientos: Los usuarios podrán consultar el detalle de sus transacciones realizadas proporcionando su número de cuenta.

----------

Formatos:
-------------
Se proporcionan los *inputs* de los servicios y la nomenclatura utilizada.

Nomenclatura:
 
- "#"  = numérico            
- "X" = texto

1	. Apertura de cuentas 
```
{
	"FirstName": "XXXX",
	"LastName": "XXXX",
	"Email": "XXXXX",
	"amount": ###
}
```
2	. Transacciones
```
{
	"idAccount": "PCOM-XXX/##",
	"transactions": [
		{
			"type": 1,
			"Description": " XX",
			"amount": ###
		},
		{
			"type": 1,
			"Description": " XX",
			"amount": ###
		},
		{
			"type": 2,
			"Description": " XXX",
			"amount": ###
		}
	]
}

```
3	. Historial
```
{
	"idAccount": "PCOM-XXX/##"
}

```


Requerimientos:
-------------

- Configura un servidor **LAMP** de forma local para verificar el funcionamiento del API.
- Base de datos: Se adjuntan sentencias sql para la configuración inicial.
```
CREATE DATABASE Testing_api;
USE Testing_api;
CREATE TABLE Users (
    ID int NOT NULL AUTO_INCREMENT,
    FirstName varchar(255) NOT NULL,
    LastName varchar(255),
    Email varchar(100),
    Password varchar(255),
    PRIMARY KEY (ID)
);
INSERT INTO Users (FirstName,LastName, Email, Password) VALUES ('Nathan', 'Smith', 'pcom@gmail.com', '3dd14afc9f2da6c03c4f6599553a4597'); 
```
> **Nota: ** Estas sentencias las puede encontrar en el archivo **query.sql** de la carpeta **bd**

- Repositorio: 
    - Debe contar con una cuenta en **bitbucket** para poder bifurcar (**fork**) el repositorio
    - Realice el examen práctico  sobre la copia que realizó
    - Al finalizar la prueba, enviar el link de su repositorio al siguiente correo: **luis.castro@propiedades.com**

Especificaciones:
-------------
- Tiene la libertad de modelar la estructura de la **BD** de acuerdo a sus necesidades, respetando los **query** que se proporciona.
- El uso de los commit debe contar con una descripción detallada.
- Al finalizar la prueba, en la carpeta **BD** crear el archivo **dump.sql** de la estructura final de la base de datos que requiere
- Estructura del repositorio:

bd
:  query.sql

web
:  index.php

**config.txt** 

-  En el archivo **config.txt**  especificar  la configuración que su examen requiere para que pueda funcionar correctamente.  Si realizo el uso de algún **framework**, favor de especificar el nombre y la versión  para su adecuado funcionamiento y la configuración básica del mismo (extensiones, rutas, etc.).



